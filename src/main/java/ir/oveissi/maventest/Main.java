package ir.oveissi.maventest;


import okhttp3.*;

import java.io.IOException;

public class Main {

    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");
    public static OkHttpClient client;

    public static void main(String[] argv) throws IOException {

        client = new OkHttpClient();
        System.out.println(get("http://moviesapi.ir/api/v1/movies"));
    }

    public static String post(String url, String json) throws IOException {
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    public static String get(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

}